import process from 'process';
import Express from 'express';


const currentTimeView = (req, res) => {
    res.send(`Server local time: ${new Date().toLocaleString()}`);
};

const setupRoutes = app => {
    app.get('/', currentTimeView);
};

const setup = () => {
    const app = Express();
    setupRoutes(app);
    const port = parseInt(process.env.VCAP_APP_PORT, 10) || 8000;
    app.listen(port, () => console.log(`listening on port ${port}`));
};

setup();