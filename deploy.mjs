import CFClient from 'cf-nodejs-client';
import Zip from 'adm-zip';

import credentials from './credentials.json'
import manifest from './manifest.json'
import { PatchedApps } from './library-patch';


const makeZip = async (files, result) => {
    const zip = new Zip();
    await Promise.all(files.map(filename => zip.addLocalFile(filename)));
    return zip.writeZip(result);
};


const getGUIDByAttribute = (allItems, attribute, value) => {
    const item = allItems.resources.find(
        x => x.entity && x.entity[attribute] === value
    )
    return item ? item.metadata.guid : '';
}


const getGUIDByName = (allItems, name) => getGUIDByAttribute(allItems, 'name', name);


const connectToCloud = async (endpoint, login, password) => {
    const cloudController = new CFClient.CloudController(endpoint);
    const usersUAA = new CFClient.UsersUAA();
    const apps = new PatchedApps(endpoint);
    const spaces = new CFClient.Spaces(endpoint);
    const routes = new CFClient.Routes(endpoint);
    const domains = new CFClient.Domains(endpoint);
    const info = await cloudController.getInfo();
    usersUAA.setEndPoint(info.authorization_endpoint);
    const token = await usersUAA.login(login, password);
    apps.setToken(token);
    spaces.setToken(token);
    domains.setToken(token);
    routes.setToken(token);
    return { apps, spaces, domains, routes };
}


const createApp = async (spaces, apps, name) => apps.add({ 
    name,
    space_guid: getGUIDByName(await spaces.getSpaces(), 'dev'),
    buildpack: 'sdk-for-nodejs',
    memory: 256,
    disk_quota: 256,
    command: 'npm run start'
});


const getOrCreateRoute = async (routes, domains, spaces, manifest) => {
    const routeGUID = getGUIDByAttribute(await routes.getRoutes(), 'host', manifest.host);
    if (routeGUID) {
        return routeGUID;
    }
    const domainGUID = getGUIDByName(await domains.getDomains(), manifest.domain);
    const spaceGUID = getGUIDByName(await spaces.getSpaces(), 'dev');
    const routeInfo = await routes.add({
        domain_guid: domainGUID,
        space_guid: spaceGUID,
        host: manifest.host
    });
    return routeInfo.metadata.guid;
}


const deploy = async () => {
    await makeZip(manifest.include, manifest.out);
    const {
        apps, spaces, domains, routes
    } = await connectToCloud(manifest.APIEndpoint, credentials.login, credentials.password);
    let appGUID = await getGUIDByName(await apps.getApps(), manifest.appName);
    if (appGUID) {
        await apps.remove(appGUID);
    }
    const response = await createApp(spaces, apps, manifest.appName);
    appGUID = response.metadata.guid;
    await apps.upload(appGUID, manifest.out, false);
    await apps.start(appGUID);
    try {
        await apps.associateRoute(
            appGUID,
            await getOrCreateRoute(routes, domains, spaces, manifest)
        )
    } catch(e) {
        console.log('Route troubles', e);
    };
    console.log(`http://${manifest.host}.${manifest.domain}`)
}


deploy();
