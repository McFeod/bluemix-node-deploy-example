import CFClient from 'cf-nodejs-client';
import restler from 'restler';
import fs from 'fs';

const readFileAsync = filename => new Promise((resolve, reject) => {
    fs.readFile(filename, function (err, data) {
        if (err)
            reject(err);
        else
            resolve(data);
    });
});

export class PatchedApps extends CFClient.Apps {
    async upload (appGuid, filePath, async) {
        const url = `${this.API_URL}/v2/apps/${appGuid}/bits`;
        const zipResources = [];
        let asyncFlag = false;

        if (typeof async === "boolean") {
            if (async) {
                asyncFlag = true;
            }
        }

        const fileData = await readFileAsync(filePath);
        const options = {
            multipart: true,
            accessToken: this.UAA_TOKEN.access_token,
            query: {
                guid: appGuid,
                async: asyncFlag
            },
            data: {
                resources: JSON.stringify(zipResources),
                application: restler.data(filePath, "application/zip", fileData)
            }
        };

        return this.REST.upload(url, options, this.HttpStatus.CREATED, false);
    }
}
